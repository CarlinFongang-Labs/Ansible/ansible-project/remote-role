#Ansible | Création de roles Ansible et mise en place des roles distant via Gitlab.

_______


><img src="https://i.pinimg.com/280x280_RS/6b/68/be/6b68bed191fdd2fad36e4193e64764ee.jpg" width="50" height="50" alt="Carlin Fongang"> 

> **Carlin FONGANG**  | fongangcarlin@gmail.com

>[LinkedIn](https://www.linkedin.com/in/carlinfongang/) | [GitLab](https://gitlab.com/carlinfongang) | [GitHub](https://github.com/carlinfongang) | [Credly](https://www.credly.com/users/carlin-fongang/badges)

_______

## Contexte
Notre plan d'action a pour but d'améliorer la collaboration avec une autre équipe en transformant notre playbook en un ensemble de rôles modulaires pour une adaptation aisée. Nous intégrerons également un playbook de tests pour faciliter son évaluation et son intégration rapide dans les processus de déploiement existants. De plus, nous créerons un dépôt privé pour nos rôles, permettant ainsi une gestion centralisée pour toute utilisation future. Ces mesures renforceront notre workflow et augmenteront notre capacité à répondre aux besoins spécifiques de l'équipe.

## Objectifs

Dans ce lab, nous allons :

- Rediger des rôles permettant de deployer des conteneurs docker. Cette étape permettra une modularité accrue, offrant ainsi la flexibilité nécessaire pour une adaptation contextuelle.

- Intégrer deux playbook de tests (wordpress et httpd) au rôle pour faciliter son évaluation rapide et son intégration dans le processus de déploiement existant.

- Provisionner une instance cible ec2 à l'aide de terraform et configurer cette instance à l'aide d'ansible et y deployer le nouveau playbook en guise de test.

- Établir une galaxy privée destinée à la conservation et à la gestion centralisée des rôles développés, renforçant l'efficacité et la réutilisation des ressources au sein de notre entreprise.

Ces actions permettront d'optimiser notre workflow et de répondre aux exigences spécifiques de l'équipe.

[Lien vers le playbook à transformer en rôle](https://gitlab.com/CarlinFongang-Labs/Ansible/lab8-role-wordpress.git)
## Prérequis
Disposer d'un machine avec avec ansible déjà installées.

Dans notre cas, nous allons provisionner une instances EC2 s'exécutant sous Ubuntu grace au provider AWS, à partir delaquelle nous effectuerons toutes nos opérations.

Documentation complémentaire :

[Documentation Ansible](https://docs.ansible.com/ansible/latest/index.html)

[Provisionner une instance EC2 sur AWS à l'aide de Terraform](https://gitlab.com/CarlinFongang-Labs/Terraform/lab2-terraform-aws)

[Provisionner une instance EC2 sur AWS à l'aide d'Ansible](https://gitlab.com/CarlinFongang-Labs/Ansible/lab10-deploy-ec2)


## 1. Refactorisation de notre playbook initial en rôle

Rappellons que nous avons mis en place un playbook permettant de lancer un conteneur [Apache consultable ici](https://gitlab.com/CarlinFongang-Labs/Ansible/lab5-playbook.git), et un autre permettant de deployer des conteneur pour [Wordpress conultable ici](https://gitlab.com/CarlinFongang-Labs/Ansible/lab8-role-wordpress.git).

Dans la suite, nous allons transformer les différentes taches du playbook necessaire au deploiement d'un conteneur (par exemple Apache/httpd), en rôle, ainsi nous dechangerons le fichier de playbook de la description de ces taches et nous ferrons simplement appel aux roles, dont nous pourrons modifier le comportement via des variables. 

## 2. Définition des taches

Pour déployer un conteneur, il faut suivre des étapes préparatoires essentielles : 

1. Préparer l'hôte Ansible en installant les paquets nécessaires.

2. Installer Terraform sur la machine Ansible pour gérer le provisionnement de l'infrastructure AWS.

3. Utiliser Terraform pour provisionner l'infrastructure et récupérer l'IP de l'instance créée.

4. Vérifier la connectivité SSH avec la nouvelle instance avant toute opération.

5. Installer les paquets requis, dont Docker, sur l'instance cible.

6. Copier le template Jinja depuis Ansible vers la machine distante.

7. Déployer le conteneur en utilisant le template Jinja et monter les volumes appropriés.

## 3. Création des différents roles

Le playbook initial dont on va s'inspirer pour réaliser nos roles se trouve [ici : Déployer un conteneur Apache (httpd) à l'aide d'un palybook Ansible](https://gitlab.com/CarlinFongang-Labs/Ansible/lab5-playbook.git)

Afin de respecter les normes de création des rôles sous Ansible, nous allons organiser notre projet (**local-role**) en sous repertoire avec une nommenclature spécique comme décrit ci-dessous :

>![Alt text](img/image.png)

Pour donner un peu de contexte : 
Dans un rôle Ansible, chaque dossier et fichier a un but spécifique pour organiser et exécuter les tâches de configuration :

- **tasks/main.yml** : Contient les principales tâches à exécuter par le rôle.
- **handlers/main.yml** : Définit les handlers, des tâches spéciales exécutées en réponse à d'autres tâches.
- **defaults/main.yml** : Spécifie les valeurs par défaut des variables utilisées dans le rôle.
- **vars/main.yml** : Stocke les variables du rôle qui ne devraient pas être modifiées par l'utilisateur.
- **files/** : Contient les fichiers statiques à copier sur les hôtes cibles.
- **templates/** : Stocke les templates Jinja2 qui seront traités et copiés sur les hôtes.
- **meta/main.yml** : Contient les métadonnées du rôle, comme les dépendances.
- **tests/inventory** et **tests/test.yml** : Utilisés pour tester le rôle.

Cette structure aide à la modularité et à la réutilisation des manifest Ansible dans un contexte de travail collaboratif.

Dans notre cas nous mettrons en place et exploiterons une partie de ces repertoires.


## 4. Définition de playbook intégrant les roles distant (hébergés sur gitlab)

Pour déployer de nouveaux playbooks utilisant des rôles définis précédemment, mais en remote, semblable à "Ansible Galaxy", ces rôles seront hébergés sur GitLab à l'adresse : [RolesHub](https://gitlab.com/CarlinFongang-Labs/Ansible/Roleshub).

Après avoir hébergé les rôles sur GitLab, le projet sera restructuré en actualisant le fichier  **roles/requirement.yml** avec les sources des rôles et en supprimant les sous-répertoires inutiles du dossier **roles**. La nouvelle structure du projet sera ainsi simplifiée.

La nouvelle strucutre du projet sera alors la suivante : 

![Alt text](img/image-26.png)

1. Définintion du fichier **roles/requirement.yml**

````bash
roles:
  - src: https://gitlab.com/CarlinFongang-Labs/Ansible/Roleshub/setup_environment.git
    scm: git
    version: "main"
    name: setup_environment

  - src: https://gitlab.com/CarlinFongang-Labs/Ansible/Roleshub/install_terraform.git
    scm: git
    version: "main"
    name: install_terraform

  - src: https://gitlab.com/CarlinFongang-Labs/Ansible/Roleshub/provide_compute.git
    scm: git
    version: "main"
    name: provide_compute

  - src: https://gitlab.com/CarlinFongang-Labs/Ansible/Roleshub/check_ssh.git
    scm: git
    version: "main"
    name: check_ssh

  - src: https://gitlab.com/CarlinFongang-Labs/Ansible/Roleshub/prepare_docker.git
    scm: git
    version: "main"
    name: prepare_docker

  - src: https://gitlab.com/CarlinFongang-Labs/Ansible/Roleshub/copy_template.git
    scm: git
    version: "main"
    name: copy_template

  - src: https://gitlab.com/CarlinFongang-Labs/Ansible/Roleshub/deploy_container.git
    scm: git
    version: "main"
    name: deploy_container

````

Ce fichier décrit plusieurs rôles Ansible, chacun provenant de dépôts GitLab, avec des informations sur le type de gestion de configuration (Git), la version (tous sur "main"), et leurs noms. Ces rôles couvrent diverses tâches comme la configuration de l'environnement, l'installation de Terraform, la vérification de la connectivité SSH, la préparation de Docker, la copie de modèles de site web, et le déploiement de conteneurs Docker, facilitant la gestion automatisée d'infrastructures et la réutilisabilité des roles.

2. Exécution du playbook

````bash
ansible-galaxy install -r roles/requirements.yml -f
````

>![Alt text](img/image-21.png)

````bash
ansible-playbook deploy_httpd.yml -k -vvv
````

>![Alt text](img/image-22.png)
*Déploiment du conteneur httpd achevé*

>![Alt text](img/image-23.png)
*Le conteneur est en cours d'exécution*

>![Alt text](img/image-24.png)
*L'application est consultable depuis le navigateur*

Dépôt du projet utilisant les roles en remote localement :  [Local Roles](https://gitlab.com/CarlinFongang-Labs/Ansible/ansible-project/local-role.git)

